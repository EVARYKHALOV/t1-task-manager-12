package ru.t1.varykhalov.tm;

import ru.t1.varykhalov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}