package ru.t1.varykhalov.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void startTaskByIndex();

    void startTaskById();

    void completeTaskByIndex();

    void completeTaskById();

}