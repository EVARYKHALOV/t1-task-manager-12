package ru.t1.varykhalov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();

}