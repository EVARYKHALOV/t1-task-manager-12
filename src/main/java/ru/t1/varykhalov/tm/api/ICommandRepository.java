package ru.t1.varykhalov.tm.api;

import ru.t1.varykhalov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}